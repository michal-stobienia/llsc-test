#ifndef LLSC_HEADER

#define LL bm_ll32
#define LL64 bm_ll64
#define SC bm_sc32
#define SC64 bm_sc64
#define STORE bm_store32
#define STORE64 bm_store64
#define INIT bm_init

// Load-linked
int bm_ll32(int* ptr);
long bm_ll64(long* ptr);
// Store-conditional
void bm_sc32(int* fail, int* ptr, int val);
void bm_sc64(int* fail, long* ptr, long val);
// Store
void bm_store32(int* ptr, int val);
void bm_store64(long* ptr, long val);
// Initialization helper
void bm_init();

#endif
#define LLSC_HEADER