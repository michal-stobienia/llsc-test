#include "llsc.h"

#include <pthread.h>
#include <stdlib.h>

#define HT_SIZE 16

uint* bmHtable;

void bmCalcHash(long ptr);
int bmCheck();
int bmCheckLocked();
void bmSet();
void bmReset();

void bmLock();
void bmUnlock();

__thread uint* h_addr;
__thread uint h_offset;
__thread long expected;

int bm_ll32(int* ptr) {
    bmCalcHash((long)ptr);
    bmLock();
    expected = (long)*ptr;
    bmSet();
    bmUnlock();
    return (int)expected;
}

void bm_sc32(int* fail, int* ptr, int val) {
    if(!bmCheck()) {
        *fail = 1;
        return;
    }
    bmLock();
    if(!bmCheckLocked()) {
        *fail = 1;
        bmUnlock();
        return;
    }
    bmReset();
    if(!__sync_bool_compare_and_swap(ptr, (int)expected, val)) {
        *fail = 1;
        bmUnlock();
        return;
    }
    bmUnlock();
    *fail = 0;
}

void bm_store32(int* ptr, int val) {
    bmCalcHash((long)ptr);
    bmLock();
    *ptr = val;
    bmReset();
    bmUnlock();
}

long bm_ll64(long* ptr) {
    bmCalcHash((long)ptr);
    bmLock();
    expected = *ptr;
    bmSet();
    bmUnlock();
    return expected;
}

void bm_sc64(int* fail, long* ptr, long val) {
    if(!bmCheck()) {
        *fail = 1;
        return;
    }
    bmLock();
    if(!bmCheckLocked()) {
        *fail = 1;
        bmUnlock();
        return;
    }
    bmReset();
    if(!__sync_bool_compare_and_swap(ptr, expected, val)) {
        *fail = 1;
        bmUnlock();
        return;
    }
    *ptr = val;
    bmUnlock();
    *fail = 0;
}

void bm_store64(long* ptr, long val) {
    bmCalcHash((long)ptr);
    bmLock();
    *ptr = val;
    bmReset();
    bmUnlock();
}

void bm_init() {
    bmHtable = (uint*)calloc(1 << HT_SIZE, sizeof(uint));
}

void bmCalcHash(long ptr) {
    ptr >>= 4; // entries in our testbench have a size of 16
               // TODO more generalizable hash function
    h_addr = &bmHtable[((long)ptr >> 4) & ((1 << HT_SIZE) - 1)];
    h_offset = ((uint)ptr & 15) << 1;
}

void bmLock() {
    uint val, newVal;
    do {
        if((val = *h_addr) & (1 << (h_offset + 1))) {
            pthread_yield();
            continue;
        }
        newVal = val | (1 << (h_offset + 1));
    } while(!__sync_bool_compare_and_swap(h_addr, val, newVal));
}

void bmUnlock() {
    uint val, newVal;
    do {
        val = *h_addr;
        newVal = val & ~(1 << (h_offset + 1));
    } while(!__sync_bool_compare_and_swap(h_addr, val, newVal));
}

void bmSet() {
    uint val, newVal;
    do {
        val = *h_addr;
        newVal = val | (1 << h_offset);
    } while(!__sync_bool_compare_and_swap(h_addr, val, newVal));
}

void bmReset() {
    uint val, newVal;
    do {
        val = *h_addr;
        newVal = val & ~(1 << h_offset);
    } while(!__sync_bool_compare_and_swap(h_addr, val, newVal));
}

int bmCheck() {
    uint mask = 3 << h_offset;
    uint comp = 1 << h_offset;
    return (*h_addr & mask) == comp;
}

int bmCheckLocked() {
    return *h_addr & (1 << h_offset);
}