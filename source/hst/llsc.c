#include "llsc.h"

#include <pthread.h>
#include <stdlib.h>

#define HT_SIZE 16

uint* htable;

#ifdef CONFLICT_RATIO
long scCount; // Number of SC operations
long fpCount; // Number of fails
#endif

void calcHash(void* ptr);

int htableCheck();
void htableSet();
void lock();
void unlock();

__thread uint* llsc_addr;

int hst_ll32(int* ptr) {
    calcHash(ptr);
    lock();
    htableSet();
    unlock();
    return *ptr;
}

void hst_sc32(int* fail, int* ptr, int val) {
    #ifdef CONFLICT_RATIO
    __sync_add_and_fetch(&scCount, 1);
    #endif
    lock();
    if(!htableCheck()) {
        unlock();
        *fail = 1;
        #ifdef CONFLICT_RATIO
        __sync_add_and_fetch(&fpCount, 1);
        #endif
        return;
    }
    htableSet();
    *ptr = val;
    *fail = 0;
    unlock();
}

void hst_store32(int* ptr, int val) {
    calcHash(ptr);
    lock();
    htableSet();
    *ptr = val;
    unlock();
}

long hst_ll64(long* ptr) {
    calcHash(ptr);
    lock();
    htableSet();
    unlock();
    return *ptr;
}

void hst_sc64(int* fail, long* ptr, long val) {
    #ifdef CONFLICT_RATIO
    __sync_add_and_fetch(&scCount, 1);
    #endif
    lock();
    if(!htableCheck()) {
        unlock();
        *fail = 1;
        #ifdef CONFLICT_RATIO
        __sync_add_and_fetch(&fpCount, 1);
        #endif
        return;
    }
    htableSet();
    *ptr = val;
    *fail = 0;
    unlock();
}

void hst_store64(long* ptr, long val) {
    calcHash(ptr);
    lock();
    htableSet();
    *ptr = val;
    unlock();
}

void hst_init() {
    htable = (uint*)calloc(2 << HT_SIZE, sizeof(uint));
    for(uint i = 0; i < 1 << HT_SIZE; ++i) {
        uint* ptr = &htable[i << 1];
        *ptr = 0;
        pthread_spin_init((pthread_spinlock_t*)(ptr + 1), 1);
    }
}

void calcHash(void* ptr) {
    ulong a = (ulong)ptr;
    a >>= 4;
    llsc_addr = (&htable[((uint)a & ((1 << HT_SIZE) - 1)) << 1]);
}

int htableCheck() {
    return *llsc_addr == (uint)pthread_self();
}

void htableSet() {
    *llsc_addr = (uint)pthread_self();
}

void lock() {
    pthread_spin_lock((pthread_spinlock_t*)(llsc_addr + 1));
}

void unlock() {
    pthread_spin_unlock((pthread_spinlock_t*)(llsc_addr + 1));
}

#ifdef CONFLICT_RATIO
float hst_getConflictRatio() {
    return (float)fpCount / (float)scCount;
}
#endif
