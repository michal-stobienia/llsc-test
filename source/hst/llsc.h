#ifndef LLSC_HEADER

#define LL hst_ll32
#define LL64 hst_ll64
#define SC hst_sc32
#define SC64 hst_sc64
#define STORE hst_store32
#define STORE64 hst_store64
#define INIT hst_init
#ifdef CONFLICT_RATIO
#define GET_CONFLICT_RATIO hst_getConflictRatio
#endif

// Load-linked
int hst_ll32(int* ptr);
long hst_ll64(long* ptr);
// Store-conditional
void hst_sc32(int* fail, int* ptr, int val);
void hst_sc64(int* fail, long* ptr, long val);
// Store
void hst_store32(int* ptr, int val);
void hst_store64(long* ptr, long val);
// Initialization helper
void hst_init();
#ifdef CONFLICT_RATIO
// Returns the ratio of false positives to overall count of SC operations
float hst_getConflictRatio();
#endif

#endif
#define LLSC_HEADER