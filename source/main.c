#undef LL

#ifdef CAS
#include "cas/llsc.h"
#endif

#ifdef HST
#include "hst/llsc.h"
#endif

#ifdef HST_BM
#include "hst-bm/llsc.h"
#endif

#ifndef LL
#error No implementation selection specified
#endif

#define NUM_THREADS 16

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

struct StackEntry {
    struct StackEntry* next;
    int value;
};

struct Stack {
    struct StackEntry* top;
};

struct Stack* _stack;

void Push(struct Stack* stack, int value) {
    int fail = 0;
    do{
        struct StackEntry* old_top = (struct StackEntry*)LL64((long*)&stack->top);
        struct StackEntry* new_top = calloc(1, sizeof(struct StackEntry));
        new_top->next = old_top;
        new_top->value = value;
        SC64(&fail, (long*)&stack->top, (long)new_top);
        if(fail) {
            free(new_top);
        }
    } while(fail);
}

int Pop(struct Stack* stack) {
    int fail = 0;
    int value = 0;
    do{
        struct StackEntry* old_top = (struct StackEntry*)LL64((long*)&stack->top);
        if(!old_top) {
            return 0;
        }
        value = old_top->value;
        struct StackEntry* new_top = old_top->next;
        SC64(&fail, (long*)&stack->top, (long)new_top);
        if(!fail) {
            free(old_top);
        }
    } while(fail);
    return value;
}

void Print(struct Stack* stack) {
    struct StackEntry* entry = stack->top;
    printf("Top = %8lX\n", (long)entry);
    while(entry) {
        printf("  %8lX: val = %i, next = %8lX\n", (long)entry, entry->value, (long)entry->next);
        entry = entry->next;
    }
}

void* worker(void* arg) {
    int l = 1000000;
	printf("thread starts, tid = %8lX\n", pthread_self());
    while(--l) {
        Push(_stack, Pop(_stack));
    }
	printf("thread ends, tid = %8lX\n", pthread_self());
    return 0;
}

void init(int num) {
    for(int i = 0; i < num; ++i) {
        Push(_stack, i + 2);
    }
}

int main(int argc, const char** argv) {
    
    #ifdef INIT
    INIT();
    #endif

    struct Stack stack;
    stack.top = 0;
    _stack = &stack;

    init(NUM_THREADS + 10);

    pthread_t threads[NUM_THREADS];

    for(int i = 0; i < NUM_THREADS; ++i) {
        pthread_create(&threads[i], 0, &worker, 0);
    }

    for(int i = 0; i < NUM_THREADS; ++i) {
        pthread_join(threads[i], 0);
    }

    Print(&stack);
    #ifdef GET_CONFLICT_RATIO
    printf("Fail ratio: %f\n", GET_CONFLICT_RATIO());
    #endif
    return 0;
}