#include "llsc.h"

__thread long value = 0;

int cas_ll32(int* ptr) {
    value = (long)*ptr;
    return value;
}

void cas_sc32(int* fail, int* ptr, int val) {
    *fail = !__sync_bool_compare_and_swap(ptr, (int)value, val);
}

void cas_store32(int* ptr, int val) {
    *ptr = val;
}

long cas_ll64(long* ptr) {
    value = *ptr;
    return value;
}

void cas_sc64(int* fail, long* ptr, long val) {
    *fail = !__sync_bool_compare_and_swap(ptr, value, val);
}

void cas_store64(long* ptr, long val) {
    *ptr = val;
}