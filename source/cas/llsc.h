#ifndef LLSC_HEADER

#define LL cas_ll32
#define LL64 cas_ll64
#define SC cas_sc32
#define SC64 cas_sc64
#define STORE cas_store32
#define STORE64 cas_store64

// Load-linked
int cas_ll32(int* ptr);
long cas_ll64(long* ptr);
// Store-conditional
void cas_sc32(int* fail, int* ptr, int val);
void cas_sc64(int* fail, long* ptr, long val);
// Store
void cas_store32(int* ptr, int val);
void cas_store64(long* ptr, long val);

#endif
#define LLSC_HEADER