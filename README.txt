A small program to test the implementations of Load-Linked and Store-Conditional instructions in x86 instruction set, as proposed by "Enhancing Atomic Instruction Emulation for Cross-ISA Dynamic Binary Translation"[1] (artifacts available under https://github.com/NKU-EmbeddedSystem/ABA-LLSC).

Compiling:
1) The program has been compiled using GCC 10.3.0 under Ubuntu 20.10. If using different platforms, make sure the platform provides POSIX threads.
2) To choose the implementation to compile, edit CMakeLists.txt to change the preprocessor defines:
    *) CAS: value-based test using compare-and-swap atomic operator (FAILS WITH ABA PROBLEM)
    *) HST: Hash Table-based test proposed by [1] (SUCCEEDS)
    *) HST_BM: Extension to the HST scheme intended to replace thread ID's with Boolean flags (FAILS WITH ABA PROBLEM)
3) If compiling with HST, CONFLICT_RATIO can be defined to store and print out the failure ratio of SC calls.

Benchmarking method:
An implementation of lock-free stack based on [1] is provided.

Extending the code:
*) While implementing own test, use the macros LL, LL64, SC, SC64, STORE and STORE64, that are replaced with functions provided by the chosen implementation.
*) Consult the comments in one of the `llsc.h` header files if willing to add an own implementation.
